using CSV, Flux, DSP, DataFrames, Flux.Tracker, Statistics
using Embeddings, TextAnalysis, Languages, WordTokenizers
using MLDataUtils, Plots, TextAnalysis
using Flux: onehotbatch, onecold, crossentropy, throttle
using Printf, MLBase
using CuArrays
using TSne
using BSON: @save
using BSON

DATA = "./data/"

GlobalMeanPool(x) = dropdims(mean(x,dims=1),dims=(1,2))

# LeNet-like architecture
model = Chain(
    Conv((3,1),300=>64, relu), MaxPool((3,1)),
    Conv((3,1),64=>96, relu), MaxPool((3,1)),
    Conv((3,1),96=>128, relu), MaxPool((3,1)),
    Conv((3,1),128=>192, relu), GlobalMeanPool,
    Dense(192,40,relu), Dense(40,3, σ), softmax)

# Define training data

good = reshape(Float32[1;0;0],(3,1))
review = reshape(Float32[0;1;0],(3,1))
bad = reshape(Float32[0;0;1],(3,1))
mp = [good,review,bad]

# get texts
tabl = DataFrame(CSV.read("Y.csv"))
# get embeddings
emb = load_embeddings(FastText_Text{:ru}; max_vocab_size=100000)

nokey=0
ws = zeros(size(emb.embeddings,2))
function embed(t,wmap)
    if haskey(wmap,t)
        return emb.embeddings[:,wmap[t]]
    else
        return undef
    end
end

unsq = Flux.unsqueeze
data = []
for a in eachrow(tabl)[1: end-11]
    txt = StringDocument(read(joinpath(DATA,"$(a.id).txt"))|> String)
    language!(txt, Languages.Russian())
    remove_corrupt_utf8!(txt)
    toks = tokens(txt)
    wmap = Dict(zip(emb.vocab,1 : length(emb.vocab)))
    vrepr = collect(hcat(filter(x->x!==undef, [embed(t,wmap) for t in toks])...))
    svr = size(vrepr')
    push!(data, (copy(reshape(vrepr',(svr[1],1,svr[2],1))), mp[1+a.pcDecision_id]))
end

ftt = open("fasttext_train.txt","w+")
fttest = open("fasttext_test.txt","w+")
function to_ft_file(ftt, l, u, lab = true)
    mp2 = Dict([0=>"__label__ok",
                1=>"__label__stand",
                2=>"__label__nok"])
    for a in eachrow(tabl)[l:u]
        txt = StringDocument(read(joinpath(DATA,"$(a.id).txt"))|> String)
        language!(txt, Languages.Russian())
        remove_corrupt_utf8!(txt)
        txt = replace(TextAnalysis.text(txt), "\n"=>" ")
        if lab
            write(ftt,mp2[a.pcDecision_id]," ", txt,"\n")
        else
            write(ftt,txt,"\n")
        end
    end
end
#
# to_ft_file(ftt,1, 61)
# to_ft_file(fttest,61, 84, false)
# #
# f = split( String(read("fastText/out")), "\n" )
# ans = [mp2i[a] for a in f[1:end-1]]
# gttrain = [r.pcDecision_id for r in eachrow(tabl)[1:59]]
# gttest = [r.pcDecision_id for r in eachrow(tabl)[61:81]]

## Plot tsne
flat(x)= reshape(x, prod(size(x)))
cbow = hcat([flat(mean(data[i][1],dims=1)) for i in 1:size(data,1)]...)
tsn = tsne(cbow') # embed into 2d space
cls = [argmax(d[2])[1] for d in data]
p1 = scatter(tsn[cls.==1,1], tsn[cls.==1,2], label = "Accept")
p1 = scatter!(p1,tsn[cls.==2,1], tsn[cls.==2,2], label="To Stand session")
p1 = scatter!(p1,tsn[cls.==3,1], tsn[cls.==3,2], label="Reject")
savefig(p1,"ft.pdf")

loss(x, y) = Flux.crossentropy(model(x),y)
accuracy(x, y) = mean(onecold.(model.(x)) .== onecold.(y))

train, test = gpu.(data[1:60]),gpu.(data[61:end])
x,y = gpu.([test[i][1] for i in 1:size(test,1)]),gpu.([test[i][2] for i in 1:size(test,1)])

data = gpu.(data)

second((x,y))=y
model = gpu(model)

### Traning loop
@info("Beginning training loop...")
best_acc = 0.0
last_improvement = 0
opt = gpu(Flux.Descent())
S = Set(1:84)
for inds in Kfold(84, 5)
    tinds = collect(setdiff(S,inds))
    for epoch_idx in 1:10
        global best_acc, last_improvement
        # Train for a single epoch
        Flux.train!(loss, params(model),data[inds], opt)

        # Calculate accuracy:
        acc = mean(accuracy(first.(data[tinds]), second.(data[tinds])),)
        @info(@sprintf("[%d]: Test accuracy: %.4f", epoch_idx, acc))

        # If our accuracy is good enough, quit out.
        if acc >= 0.999
            @info(" -> Early-exiting: We reached our target accuracy of 99.9%")
            break
        end

        # If this is the best accuracy we've seen so far, save the model out
        if acc > best_acc
            @info(" -> New best accuracy! Saving model out to mnist_conv.bson")
            BSON.@save "mnist_conv.bson" model epoch_idx acc
            best_acc = acc
            last_improvement = epoch_idx
        end

        # If we haven't seen improvement in 5 epochs, drop our learning rate:
        if epoch_idx - last_improvement >= 5 && opt.eta > 1e-6
            opt.eta /= 10.0
            @warn(" -> Haven't improved in a while, dropping learning rate to $(opt.eta)!")

            # After dropping learning rate, give it a few epochs to improve
            last_improvement = epoch_idx
        end

        if epoch_idx - last_improvement >= 10
            @warn(" -> We're calling this converged.")
            break
        end
    end
end


preds = (x->x[1]).(onecold.(model.(first.(data))))
ys = (x->x[1]).(onecold.(second.(data)))

cr = correctrate(ys,preds)
rocn = roc(ys,preds)

pr= precision(rocn)
rec = recall(rocn)
